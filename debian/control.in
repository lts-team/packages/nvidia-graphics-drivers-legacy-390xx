Source: nvidia-graphics-drivers${nvidia:Variant}
Section: non-free/libs
Priority: optional
Maintainer: Debian NVIDIA Maintainers <pkg-nvidia-devel@lists.alioth.debian.org>
Uploaders:
 Andreas Beckmann <anbe@debian.org>,
 Luca Boccassi <bluca@debian.org>,
Vcs-Browser: https://salsa.debian.org/nvidia-team/nvidia-graphics-drivers
Vcs-Git: https://salsa.debian.org/nvidia-team/nvidia-graphics-drivers.git${Vcs-Git:Branch}
Build-Depends:
 debhelper-compat (= 12),
 dpkg-dev (>= 1.18.8),
 dkms,
 libvulkan1 (>= 1.0.42),
 libxext6 [!ppc64el],
 po-debconf,
 quilt,
 xz-utils,
 linux-headers-amd64 [amd64] <!nocheck>,
 linux-headers-686 [i386] <!nocheck>,
 linux-headers-armmp [armhf] <!nocheck>,
 libglvnd-dev,
Rules-Requires-Root: no
Standards-Version: 4.6.1
Homepage: https://www.nvidia.com
Testsuite: autopkgtest-pkg-dkms
XS-Autobuild: yes

Package: ${nvidia}-driver
Section: non-free/x11
Architecture: i386 amd64 armhf
Pre-Depends:
 nvidia-installer-cleanup,
 ${nvidia:legacy-check},
Depends:
 ${nvidia}-driver-libs (= ${binary:Version}) | ${nvidia}-driver-libs-nonglvnd (= ${binary:Version}),
 ${nvidia}-driver-bin (= ${binary:Version}),
 xserver-xorg-video-${nvidia} (= ${binary:Version}),
 ${nvidia}-vdpau-driver (= ${binary:Version}),
 ${nvidia}-alternative (= ${binary:Version}),
 ${nvidia:kmod:binary},
 nvidia-support,
 ${misc:Depends}
Recommends:
 ${nvidia-settings},
# help nvidia-persistenced by pre-selecting the correct provider for libnvidia-cfg1-any
 lib${nvidia}-cfg1 (= ${binary:Version}),
 nvidia-persistenced,
Suggests:
 ${nvidia:kmod:source},
Provides:
 nvidia-driver-any,
 nvidia-glx-any,
Description: NVIDIA metapackage${nvidia:VariantDesc}
 This metapackage depends on the NVIDIA binary driver and libraries
 that provide optimized hardware acceleration of
 OpenGL/GLX/EGL/GLES/Vulkan applications via a direct-rendering X Server.
 .
 ${nvidia:kmod:Description}
 .
 ${nvidia:Models}
 .
 See /usr/share/doc/${nvidia}-driver/README.txt.gz
 for a complete list of supported GPUs and PCI IDs.
 .
 Building the kernel module has been tested up to Linux ${nvidia:kmod:tested:Linux}.

Package: ${nvidia}-alternative
Architecture: i386 amd64 armhf
Multi-Arch: foreign
Pre-Depends:
 dpkg (>= 1.17.21),
 ${nvidia:legacy-check},
 ${misc:Pre-Depends}
Depends:
 glx-alternative-nvidia (>= 0.8.4~) [!ppc64el],
 ${misc:Depends}
Provides:
 nvidia-alternative-any,
 ${nvidia}-alternative--kmod-alias,
Conflicts:
 libgldispatch0-nvidia,
 libglvnd0-nvidia,
 libopengl0-glvnd-nvidia,
 libglx0-glvnd-nvidia,
 libgl1-glvnd-nvidia-glx,
 libegl1-glvnd-nvidia,
 libgles1-glvnd-nvidia,
 libgles2-glvnd-nvidia,
Description: allows the selection of NVIDIA as GLX provider${nvidia:VariantDesc}
 In setups with several NVIDIA driver versions installed (e.g. current and
 legacy) this metapackage registers an alternative to allow easy switching
 between the different versions.
 .
 Use 'update-glx --config nvidia' to select a version.
 .
 This package does not depend on the corresponding NVIDIA libraries.
 In order to install the NVIDIA driver and libraries, install the
 ${nvidia}-driver package instead.

Package: ${nvidia-kernel}-support
Section: non-free/kernel
Architecture: i386 amd64 armhf
Multi-Arch: foreign
Depends:
 ${nvidia}-alternative (= ${binary:Version}),
 ${nvidia}-alternative--kmod-alias,
 nvidia-kernel-common (>= 20151021) [!ppc64el],
 nvidia-modprobe (>= ${nvidia:Version:major}),
 ${misc:Depends}
Provides:
 nvidia-kernel-support-any,
 ${nvidia-kernel}-support--v1,
Breaks:
 glx-alternative-nvidia (<< 0.6.91),
Replaces:
 glx-alternative-nvidia (<< 0.6.91),
Description: NVIDIA binary kernel module support files${nvidia:VariantDesc}
 The NVIDIA binary driver provides optimized hardware acceleration of
 OpenGL/GLX/EGL/GLES applications via a direct-rendering X Server
 for graphics cards using NVIDIA chip sets.
 .
 This package provides supporting configuration for the kernel module.

Package: ${nvidia-kernel}-dkms
Section: non-free/kernel
Architecture: i386 amd64 armhf
Multi-Arch: foreign
Pre-Depends:
 nvidia-installer-cleanup,
Depends:
 ${nvidia-kernel}-support--v1,
 ${misc:Depends}
Recommends:
 ${nvidia}-driver (>= ${nvidia:Version}) [!ppc64el] | ${libcuda1} (>= ${nvidia:Version}),
Provides:
 ${nvidia-kernel}-${nvidia:Version},
Description: NVIDIA binary kernel module DKMS source${nvidia:VariantDesc}
 This package builds the NVIDIA binary kernel modules needed by
 ${nvidia}-driver, using DKMS.
 Provided that you have the kernel header packages installed, the kernel
 module will be built for your running kernel and automatically rebuilt for
 any new kernel headers that are installed.
 .
 The NVIDIA binary driver provides optimized hardware acceleration of
 OpenGL/GLX/EGL/GLES applications via a direct-rendering X Server
 for graphics cards using NVIDIA chip sets.
 .
 ${nvidia:Models}
 .
 See /usr/share/doc/${nvidia-kernel}-dkms/README.txt.gz
 for a complete list of supported GPUs and PCI IDs.
 .
 This package contains the blobs for building kernel modules for the
 ${nvidia:kmod:blob:archlist}.
 Building the kernel modules has been tested up to Linux ${nvidia:kmod:tested:Linux}.

Package: ${nvidia-kernel}-source
Section: non-free/kernel
Architecture: i386 amd64 armhf
Depends:
 debhelper-compat (= ${nvidia:debhelper-compat}),
 quilt,
 ${misc:Depends}
Recommends:
 module-assistant,
 ${nvidia-kernel}-support--v1,
Suggests:
 ${nvidia}-driver (>= ${nvidia:Version}) [!ppc64el],
Description: NVIDIA binary kernel module source${nvidia:VariantDesc}
 This package provides the source for the NVIDIA binary kernel modules
 needed by ${nvidia}-driver in a form suitable
 for use by module-assistant.
 .
 The NVIDIA binary driver provides optimized hardware acceleration of
 OpenGL/GLX/EGL/GLES applications via a direct-rendering X Server
 for graphics cards using NVIDIA chip sets.
 .
 PLEASE read /usr/share/doc/${nvidia-kernel}-source/README.Debian.gz
 for building information. If you want the kernel module to be automatically
 installed via DKMS, install ${nvidia-kernel}-dkms instead.
 .
 ${nvidia:Models}
 .
 See /usr/share/doc/${nvidia-kernel}-source/README.txt.gz
 for a complete list of supported GPUs and PCI IDs.
 .
 This package contains the blobs for building kernel modules for the
 ${nvidia:kmod:blob:archlist}.
 Building the kernel modules has been tested up to Linux ${nvidia:kmod:tested:Linux}.

Package: xserver-xorg-video-${nvidia}
Section: non-free/x11
Architecture: i386 amd64 armhf
Pre-Depends:
 nvidia-installer-cleanup,
 ${nvidia:legacy-check},
Depends:
 ${nvidia}-alternative (= ${binary:Version}),
 nvidia-support,
 ${nvidia:xorgDepends},
 ${shlibs:Depends}, ${misc:Depends}
Recommends:
 ${nvidia}-driver (>= ${nvidia:Version}),
 ${nvidia}-vdpau-driver (>= ${nvidia:Version}),
 ${nvidia:kmod:binary},
 ${nvidia-settings},
Suggests:
 ${nvidia:kmod:source},
Provides:
 xserver-xorg-video-nvidia-any,
 ${nvidia:xorgProvides},
Breaks:
 glx-alternative-nvidia (<< 0.6.91),
Replaces:
 glx-alternative-nvidia (<< 0.6.91),
Description: NVIDIA binary Xorg driver${nvidia:VariantDesc}
 The NVIDIA binary driver provides optimized hardware acceleration of
 OpenGL/GLX/EGL/GLES applications via a direct-rendering X Server
 for graphics cards using NVIDIA chip sets.
 .
 ${nvidia:kmod:Description}
 .
 ${nvidia:Models}
 .
 See /usr/share/doc/${nvidia}-driver/README.txt.gz
 for a complete list of supported GPUs and PCI IDs.

Package: ${nvidia}-driver-bin
Section: non-free/x11
Architecture: i386 amd64 armhf
Depends:
 ${nvidia}-alternative (= ${binary:Version}),
 ${shlibs:Depends}, ${misc:Depends}
Recommends:
 ${nvidia}-driver,
Provides:
 nvidia-driver-bin-${nvidia:Version},
Conflicts:
 nvidia-driver-bin-${nvidia:Version},
Description: NVIDIA driver support binaries${nvidia:VariantDesc}
 The NVIDIA binary driver provides optimized hardware acceleration of
 OpenGL/GLX/EGL/GLES applications via a direct-rendering X Server
 for graphics cards using NVIDIA chip sets.
 .
 This package contains supporting binaries for the driver.

Package: lib${nvidia}-cfg1
Architecture: i386 amd64 armhf
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends}
Depends:
 ${nvidia}-alternative (= ${binary:Version}),
 ${shlibs:Depends}, ${misc:Depends}
Provides:
 libnvidia-cfg.so.1 (= ${nvidia:Version}),
 libnvidia-cfg1-any,
Breaks:
 libgl1-${nvidia}-glx (<< 355.11-5),
Replaces:
 libgl1-${nvidia}-glx (<< 355.11-5),
Description: NVIDIA binary OpenGL/GLX configuration library${nvidia:VariantDesc}
 The NVIDIA binary driver provides optimized hardware acceleration of
 OpenGL/GLX/EGL/GLES applications via a direct-rendering X Server
 for graphics cards using NVIDIA chip sets.
 .
 This package contains the libnvidia-cfg.so.1 runtime library.

Package: ${nvidia}-driver-libs
Architecture: i386 amd64 armhf
Multi-Arch: same
Depends:
 libgl1-${nvidia}-glvnd-glx (= ${binary:Version}),
 ${nvidia}-egl-icd (= ${binary:Version}),
 ${misc:Depends}
Recommends:
 ${nvidia}-driver-libs-i386 (= ${binary:Version}) [amd64],
 libopengl0 | libopengl0-glvnd-nvidia,
 libglx-${nvidia-}0 (= ${binary:Version}),
 libgles-${nvidia-}1 (= ${binary:Version}),
 libgles-${nvidia-}2 (= ${binary:Version}),
 lib${nvidia}-cfg1 (= ${binary:Version}),
 lib${nvidia}-encode1 (= ${binary:Version}),
 ${nvidia}-vulkan-icd (= ${binary:Version}) [amd64 i386],
Provides:
 nvidia-driver-libs-any,
Conflicts:
 libgldispatch0-nvidia,
 libglvnd0-nvidia,
 libopengl0-glvnd-nvidia,
 libglx0-glvnd-nvidia,
 libgl1-glvnd-nvidia-glx,
 libegl1-glvnd-nvidia,
 libgles1-glvnd-nvidia,
 libgles2-glvnd-nvidia,
Breaks:
 ${nvidia}-driver-libs-nonglvnd,
 libgl1-${nvidia}-glx,
 libegl1-${nvidia},
 ${nvidia}-nonglvnd-vulkan-icd,
Description: NVIDIA metapackage (OpenGL/GLX/EGL/GLES libraries)${nvidia:VariantDesc}
 This metapackage depends on the NVIDIA binary libraries
 that provide optimized hardware acceleration of
 OpenGL/GLX/EGL/GLES applications via a direct-rendering X Server.

Package: ${nvidia}-driver-libs-i386
Architecture: i386
Multi-Arch: foreign
Depends:
 ${nvidia}-driver-libs (= ${binary:Version}),
 ${misc:Depends}
Conflicts:
 libgl1-${nvidia}-glx-i386,
Description: NVIDIA metapackage (OpenGL/GLX/EGL/GLES 32-bit libraries)${nvidia:VariantDescShort}
 This metapackage helps the automatic installation of the 32-bit NVIDIA
 OpenGL/GLX/EGL/GLES libraries when installing ${nvidia}-driver-libs
 on amd64 with foreign architecture i386 enabled.

Package: ${nvidia}-driver-libs-nonglvnd
Architecture: i386 amd64 armhf
Multi-Arch: same
Depends:
 libgl1-${nvidia}-glx (= ${binary:Version}),
 libegl1-${nvidia} (= ${binary:Version}),
 ${misc:Depends}
Recommends:
 ${nvidia}-driver-libs-nonglvnd-i386 (= ${binary:Version}) [amd64],
 libglx-${nvidia-}0 (= ${binary:Version}),
 libgles-${nvidia-}1 (= ${binary:Version}),
 libgles-${nvidia-}2 (= ${binary:Version}),
 lib${nvidia}-cfg1 (= ${binary:Version}),
 ${nvidia}-nonglvnd-vulkan-icd (= ${binary:Version}) [amd64 i386],
Provides:
 nvidia-driver-libs-any,
Conflicts:
 libgldispatch0-nvidia,
 libglvnd0-nvidia,
 libopengl0-glvnd-nvidia,
 libglx0-glvnd-nvidia,
 libgl1-glvnd-nvidia-glx,
 libegl1-glvnd-nvidia,
 libgles1-glvnd-nvidia,
 libgles2-glvnd-nvidia,
Breaks:
 ${nvidia}-driver-libs,
 libgl1-${nvidia}-glvnd-glx,
 ${nvidia}-vulkan-icd,
Description: NVIDIA metapackage (non-GLVND OpenGL/GLX/EGL/GLES libraries)${nvidia:VariantDescShort}
 This metapackage depends on the (non-GLVND) NVIDIA binary libraries
 that provide optimized hardware acceleration of
 OpenGL/GLX/EGL/GLES applications via a direct-rendering X Server.

Package: ${nvidia}-driver-libs-nonglvnd-i386
Architecture: i386
Multi-Arch: foreign
Depends:
 ${nvidia}-driver-libs-nonglvnd (= ${binary:Version}),
 ${misc:Depends}
Conflicts:
 libgl1-${nvidia}-glx-i386,
Description: NVIDIA metapackage (non-GLVND OpenGL/EGL etc. 32-bit libraries)${nvidia:VariantDescShort}
 This metapackage helps the automatic installation of the 32-bit NVIDIA
 OpenGL/GLX/EGL/GLES libraries when installing
 ${nvidia}-driver-libs-nonglvnd
 on amd64 with foreign architecture i386 enabled.

Package: libglvnd0-nvidia
Architecture: #i386 amd64 armhf
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends}
Depends:
 ${shlibs:Depends}, ${misc:Depends}
Provides:
 glvnd-nvidia-abi-375.20,
Conflicts:
 libglvnd0,
 libgldispatch0,
Breaks:
 libgldispatch0-nvidia,
Replaces:
 libglvnd0,
 libgldispatch0,
 libgldispatch0-nvidia,
Description: Vendor neutral GL dispatch library -- libGLdispatch
 This is an implementation of the vendor-neutral dispatch layer for
 arbitrating OpenGL API calls between multiple vendors on a per-screen basis.
 .
 This package contains the (binary) GLVND libGLdispatch.so.0 shared library
 provided by NVIDIA.

Package: libopengl0-glvnd-nvidia
Architecture: #i386 amd64 armhf
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends}
Depends:
 ${shlibs:Depends}, ${misc:Depends}
Conflicts:
 libopengl0,
Replaces:
 libopengl0,
Description: Vendor neutral GL dispatch library -- libOpenGL
 This is an implementation of the vendor-neutral dispatch layer for
 arbitrating OpenGL API calls between multiple vendors on a per-screen basis.
 .
 This package contains the (binary) GLVND libOpenGL.so.0 stub library provided
 by NVIDIA which dispatches to vendor implementations via GLVND.

Package: libglx0-glvnd-nvidia
Architecture: #i386 amd64 armhf
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends}
Depends:
 ${nvidia}-alternative (= ${binary:Version}),
 ${shlibs:Depends}, ${misc:Depends}
Conflicts:
 libglx0,
Replaces:
 libglx0,
Description: Vendor neutral GL dispatch library -- libGLX
 This is an implementation of the vendor-neutral dispatch layer for
 arbitrating OpenGL API calls between multiple vendors on a per-screen basis.
 .
 This package contains the (binary) GLVND libGLX.so.0 stub library provided by
 NVIDIA which dispatches to vendor implementations (libGLX_*.so.0) via GLVND.

Package: libglx-${nvidia-}0
Architecture: i386 amd64 armhf
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends}
Depends:
 ${nvidia}-alternative (= ${binary:Version}),
 libglx0 | libglx0-glvnd-nvidia,
 ${shlibs:Depends}, ${misc:Depends}
Provides:
 libglx-vendor,
Description: NVIDIA binary GLX library${nvidia:VariantDesc}
 GLX ("OpenGL Extension to the X Window System") provides an interface between
 OpenGL and the X Window System as well as extensions to OpenGL itself.
 .
 See the description of the ${nvidia}-driver package
 or /usr/share/doc/libgl1-${nvidia}-glx/README.txt.gz
 for a complete list of supported GPUs and PCI IDs.
 .
 This package contains the driver specific binary GLX implementation by NVIDIA
 that is accessed via GLVND.

Package: libgl1-glvnd-nvidia-glx
Architecture: #i386 amd64 armhf
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends}
Depends:
 ${nvidia}-alternative (= ${binary:Version}),
 ${shlibs:Depends}, ${misc:Depends}
Conflicts:
 libgl1 (>> 0),
 libgl1-${nvidia}-glx,
Replaces:
 libgl1 (>> 0),
 libgl1-${nvidia}-glx,
Description: Vendor neutral GL dispatch library -- libGL
 This is an implementation of the vendor-neutral dispatch layer for
 arbitrating OpenGL API calls between multiple vendors on a per-screen basis.
 .
 This package contains the (binary) GLVND libGL.so.1 stub library provided by
 NVIDIA which dispatches to vendor implementations via GLVND.

Package: libgl1-${nvidia}-glvnd-glx
Architecture: i386 amd64 armhf
Multi-Arch: same
Depends:
 libgl1 (>= 0.2.999) | libgl1-glvnd-nvidia-glx,
 libglx-${nvidia-}0 (= ${binary:Version}),
 ${misc:Depends}
Provides:
 libgl1-nvidia-glx-any,
Description: NVIDIA binary OpenGL/GLX library (GLVND variant)${nvidia:VariantDesc}
 The NVIDIA binary driver provides optimized hardware acceleration of
 OpenGL/GLX/EGL/GLES applications via a direct-rendering X Server
 for graphics cards using NVIDIA chip sets.
 .
 See the description of the ${nvidia}-driver package
 or /usr/share/doc/libgl1-${nvidia}-glvnd-glx/README.txt.gz
 for a complete list of supported GPUs and PCI IDs.
 .
 This metapackage depends on the NVIDIA binary OpenGL/GLX implementation using
 GLVND and the corresponding GLVND loader library.

Package: libgl1-${nvidia}-glx
Architecture: i386 amd64 armhf
Multi-Arch: same
Pre-Depends:
 nvidia-installer-cleanup,
 ${nvidia:legacy-check},
 ${misc:Pre-Depends}
Depends:
 ${nvidia}-alternative (= ${binary:Version}),
 nvidia-support,
 ${shlibs:Depends}, ${misc:Depends}
Recommends:
# help ${nvidia:kmod:binary} by pre-selecting the correct recommended alternative
 ${nvidia}-driver-libs-nonglvnd (= ${binary:Version}),
 ${nvidia:kmod:binary},
Suggests:
 ${nvidia:kmod:source},
Provides:
 libgl1-nvidia-glx-any,
Conflicts:
 libgl1-${nvidia}-glvnd-glx,
Description: NVIDIA binary OpenGL/GLX library (non-GLVND variant)${nvidia:VariantDesc}
 The NVIDIA binary driver provides optimized hardware acceleration of
 OpenGL/GLX/EGL/GLES applications via a direct-rendering X Server
 for graphics cards using NVIDIA chip sets.
 .
 See the description of the ${nvidia}-driver package
 or /usr/share/doc/libgl1-${nvidia}-glx/README.txt.gz
 for a complete list of supported GPUs and PCI IDs.
 .
 This package contains the driver specific binary OpenGL/GLX implementation
 provided by NVIDIA as a non-GLVND alternative.

Package: lib${nvidia}-glcore
Architecture: i386 amd64 armhf
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends}
Depends:
 ${shlibs:Depends}, ${misc:Depends}
Provides:
 libnvidia-glcore-${nvidia:Version},
Conflicts:
 libnvidia-glcore-${nvidia:Version},
Description: NVIDIA binary OpenGL/GLX core libraries${nvidia:VariantDesc}
 The NVIDIA binary driver provides optimized hardware acceleration of
 OpenGL/GLX/EGL/GLES applications via a direct-rendering X Server
 for graphics cards using NVIDIA chip sets.
 .
 This package contains the private core libraries used by the NVIDIA
 implementation of OpenGL and GLX.

Package: libegl1-glvnd-nvidia
Architecture: #i386 amd64 armhf
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends}
Depends:
 ${nvidia}-alternative (= ${binary:Version}),
 ${shlibs:Depends}, ${misc:Depends}
Conflicts:
 libegl1 (>> 0),
 libegl1-${nvidia},
Replaces:
 libegl1 (>> 0),
 libegl1-${nvidia},
Description: Vendor neutral GL dispatch library -- libEGL
 This is an implementation of the vendor-neutral dispatch layer for
 arbitrating OpenGL API calls between multiple vendors on a per-screen basis.
 .
 This package contains the (binary) GLVND libEGL.so.1 stub library provided by
 NVIDIA which dispatches to vendor implementations (libEGL_*.so.0) via GLVND.

Package: libegl1-${nvidia}
Architecture: i386 amd64 armhf
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends}
Depends:
 ${nvidia}-alternative (= ${binary:Version}),
 libegl-${nvidia-}0 (= ${binary:Version}),
 ${shlibs:Depends}, ${misc:Depends}
Conflicts:
 ${nvidia}-driver-libs,
 libgl1-${nvidia}-glvnd-glx,
Description: NVIDIA binary EGL library (non-GLVND variant)${nvidia:VariantDesc}
 EGL provides a platform-agnostic mechanism for creating rendering surfaces
 for use with other graphics libraries, such as OpenGL|ES.
 .
 This package contains the driver specific binary EGL implementation by NVIDIA
 as a non-GLVND alternative.

Package: libegl-${nvidia-}0
Architecture: i386 amd64 armhf
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends}
Depends:
 ${nvidia}-alternative (= ${binary:Version}),
 ${shlibs:Depends}, ${misc:Depends}
Description: NVIDIA binary EGL library${nvidia:VariantDesc}
 EGL provides a platform-agnostic mechanism for creating rendering surfaces
 for use with other graphics libraries, such as OpenGL|ES.
 .
 See the description of the ${nvidia}-driver package
 or /usr/share/doc/libgl1-${nvidia}-glx/README.txt.gz
 for a complete list of supported GPUs and PCI IDs.
 .
 This package contains the driver specific binary EGL implementation provided
 by NVIDIA that is accessed via GLVND.

Package: libgles1-glvnd-nvidia
Architecture: #i386 amd64 armhf
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends}
Depends:
 ${nvidia}-alternative (= ${binary:Version}),
 ${shlibs:Depends}, ${misc:Depends}
Conflicts:
 libgles1 (>> 0),
Breaks:
 libgles1-nvidia (<< 361),
Replaces:
 libgles1 (>> 0),
 libgles1-nvidia (<< 361),
Description: NVIDIA binary OpenGL|ES 1.x GLVND stub library
 OpenGL|ES is a cross-platform API for full-function 2D and 3D graphics on
 embedded systems - including consoles, phones, appliances and vehicles.
 It contains a subset of OpenGL plus a number of extensions for the
 special needs of embedded systems.
 .
 OpenGL|ES 1.x provides an API for fixed-function hardware.
 .
 See the description of the ${nvidia}-driver package
 or /usr/share/doc/libgl1-${nvidia}-glx/README.txt.gz
 for a complete list of supported GPUs and PCI IDs.
 .
 This package contains the (binary) GLVND libGLESv1_CM.so.1 stub library
 provided by NVIDIA which dispatches to vendor implementations
 (libGLESv1_CM_*.so.1) via GLVND.

Package: libgles-${nvidia-}1
Architecture: i386 amd64 armhf
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends}
Depends:
 ${nvidia}-alternative (= ${binary:Version}),
 libgles1 (>= 0.2.999) | libgles1-glvnd-nvidia,
 lib${nvidia}-eglcore (= ${binary:Version}),
 ${shlibs:Depends}, ${misc:Depends}
Description: NVIDIA binary OpenGL|ES 1.x library${nvidia:VariantDesc}
 OpenGL|ES is a cross-platform API for full-function 2D and 3D graphics on
 embedded systems - including consoles, phones, appliances and vehicles.
 It contains a subset of OpenGL plus a number of extensions for the
 special needs of embedded systems.
 .
 OpenGL|ES 1.x provides an API for fixed-function hardware.
 .
 See the description of the ${nvidia}-driver package
 or /usr/share/doc/libgl1-${nvidia}-glx/README.txt.gz
 for a complete list of supported GPUs and PCI IDs.
 .
 This package contains the driver specific binary OpenGL|ES 1.x implementation
 by NVIDIA that is accessed via GLVND.

Package: libgles2-glvnd-nvidia
Architecture: #i386 amd64 armhf
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends}
Depends:
 ${nvidia}-alternative (= ${binary:Version}),
 ${shlibs:Depends}, ${misc:Depends}
Conflicts:
 libgles2 (>> 0),
Breaks:
 libgles2-nvidia (<< 361),
Replaces:
 libgles2 (>> 0),
 libgles2-nvidia (<< 361),
Description: NVIDIA binary OpenGL|ES 2.x GLVND stub library
 OpenGL|ES is a cross-platform API for full-function 2D and 3D graphics on
 embedded systems - including consoles, phones, appliances and vehicles.
 It contains a subset of OpenGL plus a number of extensions for the
 special needs of embedded systems.
 .
 OpenGL|ES 2.x provides an API for programmable hardware including vertex
 and fragment shaders.
 .
 See the description of the ${nvidia}-driver package
 or /usr/share/doc/libgl1-${nvidia}-glx/README.txt.gz
 for a complete list of supported GPUs and PCI IDs.
 .
 This package contains the (binary) GLVND libGLESv2.so.2 stub library
 provided by NVIDIA which dispatches to vendor implementations
 (libGLESv2_*.so.2) via GLVND.

Package: libgles-${nvidia-}2
Architecture: i386 amd64 armhf
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends}
Depends:
 ${nvidia}-alternative (= ${binary:Version}),
 libgles2 (>= 0.2.999) | libgles2-glvnd-nvidia,
 lib${nvidia}-eglcore (= ${binary:Version}),
 ${shlibs:Depends}, ${misc:Depends}
Description: NVIDIA binary OpenGL|ES 2.x library${nvidia:VariantDesc}
 OpenGL|ES is a cross-platform API for full-function 2D and 3D graphics on
 embedded systems - including consoles, phones, appliances and vehicles.
 It contains a subset of OpenGL plus a number of extensions for the
 special needs of embedded systems.
 .
 OpenGL|ES 2.x provides an API for programmable hardware including vertex
 and fragment shaders.
 .
 See the description of the ${nvidia}-driver package
 or /usr/share/doc/libgl1-${nvidia}-glx/README.txt.gz
 for a complete list of supported GPUs and PCI IDs.
 .
 This package contains the driver specific binary OpenGL|ES 2.x implementation
 by NVIDIA that is accessed via GLVND.

Package: lib${nvidia}-eglcore
Architecture: i386 amd64 armhf
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends}
Depends:
 ${shlibs:Depends}, ${misc:Depends}
Provides:
 libnvidia-eglcore-${nvidia:Version},
Conflicts:
 libnvidia-eglcore-${nvidia:Version},
Description: NVIDIA binary EGL core libraries${nvidia:VariantDesc}
 EGL provides a platform-agnostic mechanism for creating rendering surfaces
 for use with other graphics libraries, such as OpenGL|ES.
 .
 OpenGL|ES is a cross-platform API for full-function 2D and 3D graphics on
 embedded systems - including consoles, phones, appliances and vehicles.
 It contains a subset of OpenGL plus a number of extensions for the
 special needs of embedded systems.
 .
 This package contains the private core libraries used by the NVIDIA
 implementation of EGL and OpenGL|ES.

Package: ${nvidia}-egl-icd
Architecture: i386 amd64 armhf
Multi-Arch: same
Depends:
 nvidia-egl-common,
 libegl1 (>= 0.2.999) | libegl1-glvnd-nvidia,
 libegl-${nvidia-}0 (= ${binary:Version}),
 ${misc:Depends}
Enhances:
 libegl1,
Provides:
 libegl-vendor,
 egl-icd,
Description: NVIDIA EGL installable client driver (ICD)
 EGL provides a platform-agnostic mechanism for creating rendering surfaces
 for use with other graphics libraries, such as OpenGL|ES.
 .
 This metapackage provides the NVIDIA installable client driver (ICD) for
 EGL via GLVND which supports NVIDIA GPUs.

Package: ${nvidia}-vdpau-driver
Section: non-free/video
Architecture: i386 amd64 armhf
Multi-Arch: same
Pre-Depends:
 nvidia-installer-cleanup,
 ${nvidia:legacy-check},
 ${misc:Pre-Depends}
Depends:
 libvdpau1 (>= 0.9),
 ${nvidia}-alternative (= ${binary:Version}),
 ${shlibs:Depends}, ${misc:Depends}
Recommends:
 ${nvidia:kmod:binary},
Suggests:
 ${nvidia:kmod:source},
Enhances:
 libvdpau1,
Provides:
 vdpau-driver,
Description: Video Decode and Presentation API for Unix - NVIDIA driver${nvidia:VariantDescShort}
 These libraries provide the Video Decode and Presentation API for Unix.
 They provide accelerated video playback (incl. H.264) for the supported
 graphics cards.
 .
 This package contains the NVIDIA VDPAU driver.
 .
 See /usr/share/doc/${nvidia}-vdpau-driver/README.txt.gz
 for more information.
 .
 ${nvidia:kmod:Description}

Package: ${nvidia}-vulkan-icd
Architecture: i386 amd64
Multi-Arch: same
Depends:
 nvidia-vulkan-common,
 libvulkan1 (>= 1.0.42),
 libglx-${nvidia-}0 (= ${binary:Version}),
 ${misc:Depends}
Suggests:
 vulkan-utils,
Enhances:
 libvulkan1,
Provides:
 vulkan-icd,
 nvidia-vulkan-icd-any,
Conflicts:
 ${nvidia}-nonglvnd-vulkan-icd,
Description: NVIDIA Vulkan installable client driver (ICD)${nvidia:VariantDesc}
 Vulkan is a multivendor open standard by the Khronos Group for 3D graphics.
 .
 This metapackage provides the NVIDIA installable client driver (ICD) for
 Vulkan (GLVND variant) which supports NVIDIA GPUs.

Package: ${nvidia}-nonglvnd-vulkan-icd
Architecture: i386 amd64
Multi-Arch: same
Depends:
 nvidia-nonglvnd-vulkan-common,
 libvulkan1 (>= 1.0.42),
 libgl1-${nvidia}-glx (= ${binary:Version}),
 ${misc:Depends}
Suggests:
 vulkan-utils,
Enhances:
 libvulkan1,
Provides:
 vulkan-icd,
 nvidia-vulkan-icd-any,
Conflicts:
 ${nvidia}-vulkan-icd,
Description: NVIDIA Vulkan ICD (non-GLVND variant)${nvidia:VariantDesc}
 Vulkan is a multivendor open standard by the Khronos Group for 3D graphics.
 .
 This metapackage provides the NVIDIA installable client driver (ICD) for
 Vulkan (non-GLVND variant) which supports NVIDIA GPUs.

Package: ${nvidia}-smi
Section: non-free/utils
Architecture: i386 amd64 armhf
Depends:
 ${nvidia}-alternative (= ${binary:Version}),
 lib${nvidia}-ml1 (= ${binary:Version}),
 ${shlibs:Depends}, ${misc:Depends}
Recommends:
 ${nvidia:kmod:binary},
Suggests:
 ${nvidia:kmod:source},
Description: NVIDIA System Management Interface${nvidia:VariantDesc}
 The NVIDIA Management Library (NVML) provides a monitoring and management API.
 The application "nvidia-smi" is the NVIDIA System Management Interface (NVSMI)
 and provides a command line interface to this functionality.
 .
 See the output from the --help command line option for supported models and
 further information.

Package: lib${nvidia}-ml1
Architecture: i386 amd64 armhf
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends}
Depends:
 ${nvidia}-alternative (= ${binary:Version}),
 ${shlibs:Depends}, ${misc:Depends}
Provides:
 libnvidia-ml.so.1 (= ${nvidia:Version}),
Homepage: https://developer.nvidia.com/nvidia-management-library-NVML
Description: NVIDIA Management Library (NVML) runtime library${nvidia:VariantDesc}
 The NVIDIA Management Library (NVML) provides a monitoring and management API.
 It provides a direct access to the queries and commands exposed via nvidia-smi.
 .
 This package contains the nvidia-ml runtime library.

Package: ${libcuda1}
Architecture: i386 amd64 armhf
Multi-Arch: same
Pre-Depends:
 ${nvidia:legacy-check},
 ${misc:Pre-Depends}
Depends:
 nvidia-support,
 ${nvidia}-alternative (= ${binary:Version}),
 lib${nvidia}-fatbinaryloader (= ${binary:Version}),
 ${shlibs:Depends}, ${misc:Depends}
Recommends:
 ${nvidia:kmod:binary},
 ${nvidia}-smi,
# help nvidia-persistenced by pre-selecting the correct provider for libnvidia-cfg1-any
 lib${nvidia}-cfg1 (= ${binary:Version}),
 nvidia-persistenced,
 ${libcuda1}-i386 (= ${binary:Version}) [amd64],
Suggests:
 nvidia-cuda-mps,
 ${nvidia:kmod:source},
Provides:
 libcuda.so.1 (= ${nvidia:Version}),
 libcuda1-any,
 libcuda-5.0-1,
 libcuda-5.0-1-i386 [i386],
 libcuda-5.5-1,
 libcuda-5.5-1-i386 [i386],
 libcuda-6.0-1,
 libcuda-6.0-1-i386 [i386],
 libcuda-6.5-1,
 libcuda-6.5-1-i386 [i386],
 libcuda-7.0-1,
 libcuda-7.0-1-i386 [i386],
 libcuda-7.5-1,
 libcuda-7.5-1-i386 [i386],
 libcuda-8.0-1,
 libcuda-8.0-1-i386 [i386],
 libcuda-9.0-1,
 libcuda-9.0-1-i386 [i386],
 libcuda-9.1-1,
 libcuda-9.1-1-i386 [i386],
Homepage: https://www.nvidia.com/CUDA
Description: NVIDIA CUDA Driver Library${nvidia:VariantDesc}
 The Compute Unified Device Architecture (CUDA) enables NVIDIA
 graphics processing units (GPUs) to be used for massively parallel
 general purpose computation.
 .
 This package contains the CUDA Driver API library for low-level CUDA
 programming.
 .
 Supported NVIDIA devices include GPUs starting from GeForce 8 and Quadro FX
 series, as well as the Tesla computing processors.
 .
 ${nvidia:kmod:Description}

Package: ${libcuda1}-i386
Architecture: i386
Multi-Arch: foreign
Depends:
 ${libcuda1} (= ${binary:Version}),
 ${misc:Depends}
Description: NVIDIA CUDA 32-bit runtime library${nvidia:VariantDesc}
 This metapackage helps the automatic installation of the 32-bit NVIDIA CUDA
 library when installing ${libcuda1} on amd64 with foreign
 architecture i386 enabled.

Package: lib${nvidia}-compiler
Architecture: i386 amd64
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends}
Depends:
 ${shlibs:Depends}, ${misc:Depends}
Provides:
 libnvidia-compiler-${nvidia:Version},
Conflicts:
 libnvidia-compiler-${nvidia:Version},
Description: NVIDIA runtime compiler library${nvidia:VariantDesc}
 The Compute Unified Device Architecture (CUDA) enables NVIDIA
 graphics processing units (GPUs) to be used for massively parallel
 general purpose computation.
 .
 This package contains the runtime compiler library.

Package: lib${nvidia}-fatbinaryloader
Architecture: i386 amd64 armhf
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends}
Depends:
 lib${nvidia}-ptxjitcompiler1 (= ${binary:Version}),
 ${shlibs:Depends}, ${misc:Depends}
Provides:
 libnvidia-fatbinaryloader-${nvidia:Version},
Conflicts:
 libnvidia-fatbinaryloader-${nvidia:Version},
Description: NVIDIA FAT binary loader${nvidia:VariantDesc}
 The Compute Unified Device Architecture (CUDA) enables NVIDIA
 graphics processing units (GPUs) to be used for massively parallel
 general purpose computation.
 .
 This package contains the FAT multiarchitecture binary loader.

Package: lib${nvidia}-ptxjitcompiler1
Architecture: i386 amd64 armhf
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends}
Depends:
 ${shlibs:Depends}, ${misc:Depends}
Description: NVIDIA PTX JIT Compiler library${nvidia:VariantDesc}
 The Compute Unified Device Architecture (CUDA) enables NVIDIA
 graphics processing units (GPUs) to be used for massively parallel
 general purpose computation.
 .
 This package contains the runtime PTX JIT Compiler library.

Package: lib${nvidia-if-variant}nvcuvid1
Architecture: i386 amd64 armhf
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends}
Depends:
 ${libcuda1} (= ${binary:Version}),
 ${shlibs:Depends}, ${misc:Depends}
Description: NVIDIA CUDA Video Decoder runtime library${nvidia:VariantDesc}
 The Compute Unified Device Architecture (CUDA) enables NVIDIA
 graphics processing units (GPUs) to be used for massively parallel
 general purpose computation.
 .
 The NVIDIA CUDA Video Decoder (NVCUVID) library provides an interface to
 hardware video decoding capabilities on NVIDIA GPUs with CUDA.
 .
 This package contains the nvcuvid runtime library.

Package: lib${nvidia}-encode1
Architecture: i386 amd64 armhf
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends}
Depends:
 ${libcuda1} (= ${binary:Version}),
 ${shlibs:Depends}, ${misc:Depends}
Description: NVENC Video Encoding runtime library${nvidia:VariantDesc}
 The NVENC Video Encoding library provides an interface to video encoder
 hardware on supported NVIDIA GPUs.
 .
 This package contains the nvidia-encode runtime library.

Package: lib${nvidia}-ifr1
Architecture: i386 amd64 armhf
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends}
Depends:
 ${libcuda1} (= ${binary:Version}),
 ${shlibs:Depends}, ${misc:Depends}
Description: NVIDIA OpenGL-based Inband Frame Readback runtime library${nvidia:VariantDescShort}
 The NVIDIA OpenGL-based Inband Frame Readback (NvIFROpenGL) library provides
 a high performance, low latency interface to capture and optionally encode an
 OpenGL framebuffer. NvIFROpenGL is a private API that is only available to
 approved partners for use in remote graphics scenarios.
 .
 This package contains the NvIFROpenGL runtime library.

Package: lib${nvidia}-fbc1
Architecture: i386 amd64 armhf
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends}
Depends:
 ${libcuda1} (= ${binary:Version}),
 ${shlibs:Depends}, ${misc:Depends}
Description: NVIDIA OpenGL-based Framebuffer Capture runtime library${nvidia:VariantDesc}
 The NVIDIA OpenGL-based Framebuffer Capture (NvFBCOpenGL) library provides
 a high performance, low latency interface to capture and optionally encode an
 OpenGL framebuffer. NvFBCOpenGL is a private API that is only available to
 approved partners for use in remote graphics scenarios.
 .
 This package contains the NvFBCOpenGL runtime library.

Package: ${nvidia}-opencl-icd
Architecture: i386 amd64
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends}
Depends:
 nvidia-opencl-common,
 ocl-icd-libopencl1 | nvidia-libopencl1 | libopencl1,
 ${nvidia}-alternative (= ${binary:Version}),
 ${libcuda1} (= ${binary:Version}),
 lib${nvidia}-compiler (= ${binary:Version}) [!ppc64el],
 ${shlibs:Depends}, ${misc:Depends}
Enhances:
 libopencl1,
Provides:
 opencl-icd,
Description: NVIDIA OpenCL installable client driver (ICD)${nvidia:VariantDesc}
 OpenCL (Open Computing Language) is a multivendor open standard for
 general-purpose parallel programming of heterogeneous systems that include
 CPUs, GPUs and other processors.
 .
 This package provides the NVIDIA installable client driver (ICD) for OpenCL
 which supports NVIDIA GPUs. This ICD supports OpenCL 1.x only.
